/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.ktyp.lab1;

import java.util.Scanner;

/**
 *
 * @author toey
 */
public class Lab1 {

    public String[][] list = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    public String start;
    public String turn;
    public boolean confirm;
    public int row;
    public int column;
    public boolean isEnd = false;

    Scanner sc = new Scanner(System.in);

    public void checkStart() {
        System.out.println("----- Welcome to XO Game -----");
        System.out.print("Start XO Games? (Y/N) : ");
        start = sc.nextLine().toLowerCase();
        while (!start.equals("y") && !start.equals("n")) {
            System.out.print("Start XO Games? (Y/N) : ");
            start = sc.nextLine().toLowerCase();
        }
        if (start.equals("n")) {
            confirm = false;
        } else {
            confirm = true;
        }
        chooseFirstturn();
    }

    public void chooseFirstturn() {
        System.out.print("Who first player? (X/O) : ");
        turn = sc.nextLine().toLowerCase();
        while (!turn.equals("x") && !turn.equals("o")) {
            System.out.print("Who first player? (X/O) :  ");
            turn = sc.nextLine().toLowerCase();
        }
        if (turn.equals("x")) {
            turn = "x";
        }
        if (turn.equals("o")) {
            turn = "o";
        } else {
            return;
        }
        System.out.println("--------------------------------");
    }

    public void showTable() {
        System.out.println(" ----------- ");
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list[i].length; j++) {
                System.out.print("  " + list[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println(" ----------- ");

    }

    public void showTurn() {
        System.out.println("Now turn ---> " + turn.toUpperCase());
    }

    public void inputRowandColumn() {
        System.out.print("Input row (1-3) : ");
        row = sc.nextInt();
        System.out.print("Input column (1-3) : ");
        column = sc.nextInt();
        if (((row > 0 && row < 4) && (column > 0 && column < 4))) {
            if (list[row - 1][column - 1].equals("-")) {
                list[row - 1][column - 1] = turn.toUpperCase();

            } else {
                return;
            }
            showTable();
            checkWin();
            changeTurn();
        } else {
            System.out.println("**Plese input again that Row and Column is already exits.** ");
            return;
        }
    }

    public void changeTurn() {
        if (turn.equals("x")) {
            turn = "o";
        } else {
            turn = "x";
        }
    }

    public void checkWin() {
        if (checkNawTang() || checkNawnond() || checkTayang()) {
            System.out.println(" ----- Congratulations ----- ");
            System.out.println("       The winner is " + turn.toUpperCase());
            System.out.println("--------------------------------");
            if (continute()) {
                reset();
            } else {
                isEnd = true;
            }
        }

        if (isEnd == false) {
            if (checkDraw()) {
                isEnd = true;
                System.out.println("****** Game over ******");
                System.out.println("  -----    Draw   -----  ");
                System.out.println("---------------------------");
                if (continute()) {
                    reset();
                } else {
                    isEnd = true;
                }
            }
        }
    }

    public void process() {
        showTurn();
        inputRowandColumn();
    }

    public boolean checkNawTang() {
        for (int i = 0; i < list[row - 1].length; i++) {
            if (!list[i][column - 1].toLowerCase().equals(turn)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkNawnond() {
        for (int j = 0; j < list[row - 1].length; j++) {
            if (!list[row - 1][j].toLowerCase().equals(turn)) {
                return false;
            }
        }

        return true;
    }

    public boolean checkTayang() {
        if (row - 1 == column - 1) {
            for (int i = 0; i < list.length; i++) {
                if (!list[i][i].toLowerCase().equals(turn)) {
                    return false;
                }

            }
            return true;
        }
        if ((row + column) - 2 == list.length - 1) {
            for (int i = 0; i < list.length; i++) {
                if (!list[i][list.length - 1 - i].toLowerCase().equals(turn)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list.length; j++) {
                if (list[i][j].toLowerCase().equals("-")) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean continute() {
        System.out.print("Do You want to play again (Y/N) : ");
        String str = sc.next();
        while (!str.toLowerCase().equals("n") && !str.toLowerCase().equals("y")) {
            System.out.print("Do You want to play again (Y/N) : ");
            str = sc.next().toLowerCase();
        }
        if (str.equals("n")) {
            isEnd = true;
            System.out.println("    ----- Have a nice day ----- ");
            return false;
        } 
        return true;  
    }

    public void reset() {
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list.length; j++) {
                list[i][j] = "-";
            }
        }
    }

    public static void main(String[] args) {
        Lab1 project = new Lab1();
        project.checkStart();
        if (project.confirm == false) {
            System.out.println("    ------ Goodbye ------ ");
            project.isEnd = true;
        }
        while (!project.isEnd) {
            project.process();
        }
    }

}
